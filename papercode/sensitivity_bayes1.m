clear all; close all; clc;
run startup;

% Parameters for sensitivity analysis
nruns = 200;
par = 'w/c'; 
rpar = [0 0.1];

% Parameters for optimization
rx = [0 10]'; 
nt = 600;

% Bayes opt
hyp.mean = []; hyp.meanfunc = @meanZero;
ell = 0.5; sf = 1; hyp.cov = [log(ell); log(sf)]; hyp.covfunc = @covSEiso; 
sn = 1; hyp.lik = log(sn); hyp.likfunc = @likGauss; 
hyp.opt = {'hyp.lik', 'hyp.cov(2)'};

%% Iterate sensitivity analysis

% generate values for sensitivity analysis
vals = sort(rpar(1) + diff(rpar)*rand(1,nruns));

% iterate through values
tic
parfor irun = 1:nruns
    disp([int2str(irun) ': ' par ' = ' num2str(vals(irun))])

    % set reward function
    wcs = vals(irun)*[1 1];        % Decision coefficients
    f = @(x) reward_ddf(x, wcs);
    
    % optimization
    [xopt, fopt, vopt] = opt_bayes1(f, rx, nt, hyp);    
    
    % store
    nav = 100; rav = (nt-nav):nt;
    x1(irun) = mean(xopt(rav));
    r(irun) = mean(fopt(rav));
    v1(irun) = mean(vopt(1,rav));
    v2(irun) = mean(vopt(2,rav)); 
    
end
time = toc

% Store results
save(mfilename)
load(mfilename)
clear hyp

%% Plot results

% baseline results
uiopen('sensitivity_sampling.fig',1)
hfig = findobj('type','patch');
for i = 1:length(hfig); delete(hfig(i)); end
hfig = findobj('type','line');
for i = 1:length(hfig); set(hfig(i),'color','b'); end
for i = 2:2:length(hfig); delete(hfig(i)); end

% figure; clf;
let = {'A','B','C','D'};
xlab = {par,par,par,par};
ylab = {'b','r','e','rt'};
tit = {'Threshold', 'Reward','Error','Reaction time'};
xvar = {'vals','vals','vals','vals'};
yvar = {'x1','r','v1','v2'};
col = {'.k','.k','.k','.k'};

nplots = length(yvar);
for iplot = 1:nplots
    eval(['x = ' xvar{iplot} '(:);']); lx = min([0; x]); mx = max(x)+eps;
    eval(['y = ' yvar{iplot} '(:);']); ly = min([0; y]); my = max(y)+eps;
    
    subplot(1,nplots,iplot); grid on; hold on; box on;
    plot(x, y, col{iplot});
    xlabel(xlab{iplot});
    ylabel(ylab{iplot});
    title(tit{iplot});
    axis([lx mx ly my]);
    
    hyp1.cov = [log(0.01); log(1)]; hyp1.lik = log(1);
    hyp1 = minimize(hyp1, @gp, -100, @infExact, [], @covSEiso, @likGauss, x, y);
    xtest = linspace(lx,mx,100)';
    [ytest, s2] = gp(hyp1, @infExact, [], @covSEiso, @likGauss, x, y, xtest);
    
    plot(xtest, ytest, 'r');
    fill([xtest;flipud(xtest)],[ytest+sqrt(s2);flipud(ytest-sqrt(s2))],...
        [1 0.75 0.75],'EdgeColor','r','FaceAlpha',0.1,'EdgeAlpha',0.3);
 
    if iplot==1; sd = mean(sqrt(s2)); end

end

disp([time sd])
savefig(mfilename)
