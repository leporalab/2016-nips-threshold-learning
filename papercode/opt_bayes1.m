function [xopt yopt vopt] = opt_bayes1(func, rx, nt, hyp, cov, lik, mean)
% Bayesian optimisation algorithm
% func = reward function handle
% rx = variable range
% nt = max # timesteps

if exist('cov','var'); hyp.cov = cov; end 
if exist('lik','var'); hyp.lik = lik; end 
if exist('mean','var'); hyp.mean = mean; end 

% Generate initial samples
d = size(rx,2);
n = 4 + floor(3*log(d));
xopt = repmat(rx(1,:)',1,n) + repmat(rx(2,:)'-rx(1,:)',1,n).*rand(d,n);

% Evaluate reward for initial samples
yopt = nan(1,n);
for k = 1:n, yopt(k) = func(xopt(:,k)); end

% Specify direct optimization
opt.tol = 1e-1; opt.mxevals = 50; opt.maxits = 50; opt.maxdeep = 50;
opt.testflag = 0; opt.globalmin = -Inf; opt.showits = 0;

% GP optimization options
opt_unc = optimoptions('fminunc', 'Algorithm', 'quasi-newton', 'GradObj', 'off', 'display', 'off');
new.hyp = hyp;

% Iteratively construct response surface using GP ...
for t = n+1:nt
    
    % Optimise GP hyperparameters
    if ~rem(t-1,10) && iscell(hyp.opt)
        for i = 1:length(hyp.opt); evalc(['pars(i) = ',hyp.opt{i},';']); end
        pars = fminunc(@(par) logZ(hyp_new(par, hyp), xopt, yopt), pars, opt_unc);
        
        % if hyperparameters too small gp gives error
        for i = 1:length(hyp.opt); evalc(['new.',hyp.opt{i},' = max(-5,pars(i));']); end 
    end
           
    % Specify response function
    rfunc = @(x) gp(new.hyp, @infExact, [], hyp.covfunc, hyp.likfunc, xopt', yopt', x');
    
    % Specify acquisition function
    afunc = @(x) probimp(x, rfunc, max(yopt));
    
    % Find global maximum of acquisition function
    problem.f = @(x) -afunc(x);
    evalc('[~, x] = opt_direct(problem, rx'', opt);'); % evalc suppresses output
    
    % Sample function
    if nargout(func)==1; r = func(x); v = nan;
    else [r,v] = func(x); end
    
    % Update return values
    xopt(:,t) = x;
    yopt(t) = r;
    vopt(:,t) = v;
    
    % Plot response surface
    if d == 1 && ~rem(t-1,20)
        plot_1D(rfunc, rx, xopt, yopt, hyp)
    end
    if d == 2 && ~rem(t-1,20)
        plot_2D(rfunc, rx, xopt, yopt, hyp)
    end
end

end

function y = probimp(x, func, T)

e = 0;
[m, s2] = func(x);
y = normcdf((m - T - e)./sqrt(s2));

end

function y = uppercb(x, func, t)

[m, s2] = func(x);
q = 5; k = 10;
kt = k - k*(1 - 1.0/t)^q;
y = m + kt*sqrt(s2);

end

function plot_1D(func, rx, xopt, yopt, hyp)

npoints = 100;
xtest = linspace(rx(1,1),rx(2,1),npoints)';
[ytest, s2] = func(xtest');

clf; hold on;
fill([xtest;flipud(xtest)],[ytest+sqrt(s2);flipud(ytest-sqrt(s2))],...
    [1 0.75 0.75],'EdgeColor','r','FaceAlpha',0.1,'EdgeAlpha',0.3);
plot(xtest, ytest, 'r', xopt, yopt, 'xk') %, xbest, ybest, 'og');

xlabel('x1'); zlabel('y');
title(['cov: ' num2str(exp(hyp.cov)','%6.2f')...
    ' lik: ' num2str(exp(hyp.lik)','%6.2f')])
drawnow

end

function plot_2D(func, rx, xopt, yopt, hyp)

npoints = 100;
[xtest1,xtest2] = meshgrid(linspace(rx(1,1),rx(2,1),npoints), ...
    linspace(rx(1,2),rx(2,2),npoints));
xtest = [xtest1(:) xtest2(:)];

ytest = func(xtest');
ytest = reshape(ytest, size(xtest1));

surf(xtest1, xtest2, ytest);
xlabel('x1'); ylabel('x2'); zlabel('y');

title(['cov: ' num2str(exp(hyp.cov)','%6.2f')...
    ' lik: ' num2str(exp(hyp.lik)','%6.2f')])
drawnow

end

function [logZ, dlogZ] = logZ(hyp, x, y)

% if hyperparameters too small logZ gives error
for i = 1:length(hyp.opt); evalc(['if ',hyp.opt{i},'<-5; ',hyp.opt{i},' = -5; end']); end

[~, logZ, dlogZ] = infExact(hyp, {hyp.meanfunc}, {hyp.covfunc}, {hyp.likfunc}, x', y');
    
end

function hyp = hyp_new(par, hyp)

for i = 1:length(hyp.opt)
    evalc([hyp.opt{i} ' = par(i);']);
end

end
 
% function plotlogZ_2D(hyp, xopt, yopt)
% 
% [x0, x1] = meshgrid(0:0.01:1); x0 = 10*x0;
% [m,n] = size(x0);
% y = nan(m,n);
% for i = 1:m
%     for j = 1:n
%         hyp.cov = [log(x0(i)); log(x1(j))];
%         y(i,j) = logZ(hyp, xopt, yopt);
%     end
% end
% 
% figure(10); 
% mesh(x0, x1, y); 
% axis square; colormap hot; 
% hold on
% 
% end