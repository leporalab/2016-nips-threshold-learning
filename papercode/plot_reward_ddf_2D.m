clear all; clc;

cs = 0.01*[1 1];
nsamples = 50;
maxt = 500;
[x0, x1] = meshgrid(0:0.05:10);

[m,n] = size(x0);
r = nan(m,n);
for i = 1:m; [i, m]
    for j = 1:n
        r(i,j) = reward_ddf([x0(i,j) x1(i,j)], cs, nsamples, maxt);
    end
end

save(mfilename)
load(mfilename)

mesh(x0, x1, r);
xlabel('theta0');
ylabel('theta1');
zlabel('reward');
axis square;
colormap hot;
colorbar;

savefig(mfilename)

