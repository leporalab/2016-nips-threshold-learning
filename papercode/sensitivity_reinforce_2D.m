clear all; close all; clc;
run startup; % ensure gpml on path

% Parameters for sensitivity analysis
nruns = 200^2;
par = 'w/c';
rpar = [0 0.1];

% Parameters for optimization
rx = [0 10; 0 10]';
nt = 5000;

%% Iterate sensitivity analysis0

% generate values for sensitivity analysis
vals = linspace(rpar(1),rpar(2),sqrt(nruns));
[i,j] = ind2sub(sqrt(nruns)*[1 1],1:nruns); vals = [vals(i); vals(j)]';

parfor irun = 1:nruns
    disp([int2str(irun) ': ' par ' = ' num2str(vals(irun,:))])
    
    % set reward function
    wcs = vals(irun,:);        % Decision coefficients
    f = @(x) reward_ddf(x, wcs);
    
    % optimization
    [xopt, fopt, vopt] = opt_reinforce(f, rx, nt);
    
    % store
    nav = nt/10; rav = (nt-nav):nt;
    x1(irun) = mean(xopt(1,rav));
    x2(irun) = mean(xopt(2,rav));
    r(irun) = mean(fopt(rav));
    v1(irun) = mean(vopt(1,rav));
    v2(irun) = mean(vopt(2,rav)); 
end

% Store results
save(mfilename)
load(mfilename)

%% Plot results

figure(3); clf
tit = {'Decision threshold','Error probability','Decision time','Reward'};
yvar = {'x1','v1','v2','r'};
lzs = [0 0 0 -0.5 0 0];
mzs = [10 0.5 30 0 10 0.5];

for iplot = 1:4
  
  x = vals(:,1); lx = min([0; x]); mx = max([0; x]);
  y = vals(:,2); ly = min([0; y]); my = max([0; y]);
  eval(['z = ' yvar{iplot} '(:);']); lz = lzs(iplot); mz = mzs(iplot);
  z = reshape(z,sqrt(nruns)*[1 1]);
  
  subplot(1,4,iplot); grid on; hold on; box on;
  imagesc(x,y,z,[lz mz]); 
  cm = colormap('hot'); cm = flipud(cm); colormap(cm);
  axis([lx,mx,ly,my])
  set(gca,'XTick',0:0.02:0.1,'YTick',0:0.02:0.1)
  rz{iplot} = linspace(lz,mz,6);
  
  set(gca,'fontsize',6)
  title(tit{iplot},'fontsize',8)
  xlabel('weight (Bayes risk), c/W_0','fontsize',6); 
  ylabel('weight (Bayes risk), c/W_1','fontsize',6);
  
end

