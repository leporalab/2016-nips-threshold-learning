function [r,v] = reward_ddf(x, wcs, nsamples, maxt)
% x = thresholds
% cs = cost parameters
% nsamples = # samples to average reward over
% maxt = max decision time

if ~exist('nsamples','var'); nsamples = 1; end
if ~exist('maxt','var'); maxt = 500; end

ntrials = 100000;
c = max(wcs); ws = wcs/c;

% if only one input
if length(x)==1
    x = [x x];
end

% pregenerate model runs
persistent target z;
if isempty(z)
    mu = 1/3*[1 -1];
    ntargets = length(mu);
    sd = 1.0*ones(ntargets,1);
    
    for itrial = 1:ntrials
        target(itrial) = randi(ntargets);
        s = mu(target(itrial)) + sd(target(itrial))*randn(maxt,1);
        z(:,itrial) = cumsum(s);
    end
end

% sample rewards
r = nan(nsamples,1); 
v = nan(nsamples,2); % variables (e,t)
for isample = 1:nsamples
    itrial = randi(ntrials);
    t_1 = find( z(:,itrial) >= x(1), 1, 'first'); if isempty(t_1); t_1 = nan; end
    t_2 = find( z(:,itrial) <= -x(2), 1, 'first'); if isempty(t_2); t_2 = nan; end
    [rt, n] = min([t_1 t_2]); if rt==maxt+2; rt = nan; n = nan; end
    e = abs(n - target(itrial));
   
    r(isample) = - c*rt - ws(target(itrial))*e;
    v(isample,:) = [e; rt];
end
r = nanmean(r); 
v = nanmean(v,1); 
end
