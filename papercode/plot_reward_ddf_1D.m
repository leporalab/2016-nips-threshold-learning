clear all; clc; close all
run startup; % ensure gpml on path

cs = 0.02*[1 1];
nsamples = 1;
maxt = 500;
nx = 1000;
x = sort(10*rand(1,nx));

% Sample reward function
for i = 1:nx, i
    y(i) = reward_ddf([x(i) x(i)], cs, nsamples, maxt);
end

% GP hyperparameters
hyp.mean = []; meanfunc = @meanZero;
ell = 1; sf = 1; hyp.cov = [log(ell); log(sf)]; covfunc = @covSEiso; 
sn = 1; hyp.lik = log(sn); likfunc = @likGauss; 

% fit GP
ntest = 1000;
xtest = linspace(floor(min(x)), ceil(max(x)), ntest);
hyp = minimize(hyp,'gp', -100, @infExact, meanfunc, covfunc, likfunc, x', y');
[ytest, s2] = gp(hyp, @infExact, meanfunc, covfunc, likfunc, x', y', xtest');

% plot sampled reward function
figure(2); clf; hold on
plot(x, y, '.k');
plot(xtest', ytest, 'r');
% fill([xtest';flipud(xtest')],[ytest+sqrt(s2);flipud(ytest-sqrt(s2))],...
%     [1 0.75 0.75],'EdgeColor','r','FaceAlpha',0.1,'EdgeAlpha',0.3);
xlabel('theta');
ylabel('reward');
% axis([0 10 floor(min(ytest)) 0])

savefig(mfilename)
save(mfilename)