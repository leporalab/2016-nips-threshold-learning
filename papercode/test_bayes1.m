clear all; close all; clc;
run startup;

%% Choose function to optimise + parameters

wcs = 0.05*[1 0.2];    % Decision coefficients
reward = @(x) reward_ddf(x, wcs);  
nsamples = 1;        % Number of samples to use to estimate reward
maxt = 500;          % Max decision time
reward = @(x) reward_ddf(x, wcs, nsamples, maxt);   

f = reward; rx = [0 10; 0 10]'; nt = 1000;
% f = reward; rx = [0 10]'; nt = 500; 

%% Run optimiser

hyp.mean = []; hyp.meanfunc = @meanZero;
ell = 0.1; sf = 1; hyp.cov = [log(ell); log(sf)]; hyp.covfunc = @covSEiso; 
sn = 1; hyp.lik = log(sn); hyp.likfunc = @likGauss; 
hyp.opt = {'hyp.lik', 'hyp.cov(2)'};

[xopt, fopt, vopt] = opt_bayes1(f, rx, nt, hyp);

% save results
save(mfilename)
load(mfilename)

%% Plot results
if size(xopt,1)==1; xopt(2,:) = xopt(1,:); end
t = 1:nt; 

% Apply smoothing filter
span = 10;
r = smooth(fopt,span);
x1 = smooth(xopt(1,:),span);
x2 = smooth(xopt(2,:),span);
v1 = smooth(vopt(1,:),span);
v2 = smooth(vopt(2,:),span);

figure; clf;
let = {'A','B','C','D','E'};
xlab = {'trials','trials','trials','trials','trials'};
ylab = {'fitness','x(1)','x(2)','e','rt'};
tit = {'Fitness','Value x(1)','Value x(2)','Error','Reaction time'};
xvar = {'t','t','t','t','t'};
yvar = {'r','x1','x2','v1','v2'};
col = {'k','b','g','k','k'};

nplots = length(yvar);
for iplot = 1:nplots;
    eval(['x = ' xvar{iplot} '(:);']); lx = min([0; x]); mx = max(x)+eps;
    eval(['y = ' yvar{iplot} '(:);']); ly = min([0; y]); my = max(y*1.1)+eps;
    subplot(1,nplots,iplot); grid on; hold on; box on;
    plot(x, y, col{iplot});
    xlabel(xlab{iplot});
    ylabel(ylab{iplot});
    title(tit{iplot});
    axis([lx mx ly my]);
end
