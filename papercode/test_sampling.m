clear all; close all; clc;
% dbstop if error

%% Choose function to optimise + parameters
% exp2 = @(x) exp(-(x - [1;1])'*(x - [1;1]));                 % Gaussian
% para2 = @(x) 4 - (x - [0.5;1])'*(x - [0.5;1]);              % Parabola
% rosen2 = @(x) -(( 1 - x(1))^2 + 100 * (x(2) - x(1)^2)^2);   % Rosenbrock
% matyas = @(x) -(0.26*(x(1)^2 + x(2)^2) - 0.48*x(1)*x(2));   % Matyas
% mccorm = @(x) -(sin(x(1)+x(2)) ...
%     + (x(1)-x(2))^2 -1.5*x(1) + 2.5*x(2) + 1);              % McCormick
% branin = @(x) -((x(2)-(5.1/(4*pi^2))*x(1)^2+5*x(1)/pi-6)^2 ...  % Branin
%     + 10*(1-1/(8*pi))*cos(x(1))+10);
% peak2d = @(x) 3*(1-x(2)).^2.*exp(-(x(2).^2) - (x(1)+1).^2) ...  % Peak2D
%     - 10*(x(2)/5 - x(2).^3 - x(1).^5).*exp(-x(2).^2-x(1).^2) ...
%     - 1/3*exp(-(x(2)+1).^2 - x(1).^2);

wcs = 0.05*[1 0.2];    % Decision coefficients
nsamples = 1000;       % Number of samples to use to estimate reward
maxt = 100;            % Max decision time
reward = @(x) reward_ddf(x, wcs, nsamples, maxt);   

% f = exp2; d = 2; xrng = [-2 -2; 2 2]; tmax = 1000;
% f = para2; d = 2; xrng = [-2 -2; 2 2]; tmax = 1000;
% f = rosen2; d = 2; xrng = [-5 -5; 10 10]; tmax = 5000;
% f = matyas; d = 2; xrng = [-10 -10; 10 10]; tmax = 5000;
% f = mccorm; d = 2; xrng = [-1.5 -3; 4 4]; tmax = 5000;
% f = branin; d = 2; xrng = [-5 0; 10 15]; tmax = 5000;
% f = peak2d; d = 2; xrng = [-3 -3; 3 3]; tmax = 5000;
% f = reward; rx = [0 10; 0 10]'; nt = 100;
% f = reward; rx = [0 10; 0 10]'; nt = 100;
f = reward; rx = [0 10]'; nt = 1000;

%% Run optimiser
% 
% hyp.mean = []; hyp.meanfunc = @meanZero;
% ell = 1; sf = 1; hyp.cov = [log(ell); log(sf)]; hyp.covfunc = @covSEiso; 
% sn = 1; hyp.lik = log(sn); hyp.likfunc = @likGauss; 
% 
% [xopt, fopt, vopt] = opt_sampling(f, rx, nt, hyp);
% 
% % save results
% save(mfilename)
% load(mfilename)
% 
% %% Plot results
% if size(xopt,1)==1; xopt(2,:) = xopt(1,:); end
% t = 1:nt; x1 = xopt(1,:); x2 = xopt(2,:);
% 
% % Apply smoothing filter
% span = 51;
% fopt = smooth(fopt,span);
% x1 = smooth(x1,span);
% x2 = smooth(x2,span);
% 
% figure; clf;
% let = {'A','B','C'};
% xlab = {'trials','trials','trials'};
% ylab = {'fitness','x(1)','x(2)'};
% tit = {'Fitness', 'Value x(1)', 'Value x(2)'};
% xvar = {'t','t','t'};
% yvar = {'fopt','x1','x2'};
% col = {'k','b','g'};
% 
% for iplot = 1:3;
%     eval(['x = ' xvar{iplot} '(:);']); lx = min([0; x]); mx = max(x)+eps;
%     eval(['y = ' yvar{iplot} '(:);']); ly = min([0; y]); my = max(y*1.1)+eps;
%     subplot(1,3,iplot); grid on; hold on; box on;
%     plot(x, y, col{iplot});
%     xlabel(xlab{iplot});
%     ylabel(ylab{iplot});
%     title(tit{iplot});
% %     axis([lx mx ly my]);
% end
