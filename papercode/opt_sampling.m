function [xopt yopt vopt] = opt_sampling(func, rx, nxs, hyp)
% Sampling optimisation algorithm
% func = reward function handle
% rx = variable range
% nt = max # timesteps

% Generate samples
d = size(rx,2);
x = repmat(rx(1,:)',1,nxs) + repmat(rx(2,:)'-rx(1,:)',1,nxs).*rand(d,nxs);

% Sample reward function
y = nan(1,nxs); v = nan(2,nxs);
if nargout(func)==1; for k = 1:nxs; v(:,k) = func(x(:,k)); end
else for k = 1:nxs; [y(k) v(:,k)] = func(x(:,k)); end; end    

% fit GP
evalc('hyp = minimize(hyp,''gp'', -100, @infExact, hyp.meanfunc, hyp.covfunc, hyp.likfunc, x'', y'')');

% Specify response function
rfunc = @(xsample) gp(hyp, @infExact, hyp.meanfunc, hyp.covfunc, hyp.likfunc, x', y', xsample');

% Find global maximum of acquisition function
[xopt, yopt] = find_max(rx, rfunc);

% Estimate other parameters
v1 = v(1,:); v2 = v(2,:);
vfunc1 = @(xsample) gp(hyp, @infExact, hyp.meanfunc, hyp.covfunc, hyp.likfunc, x', v1', xsample');
vfunc2 = @(xsample) gp(hyp, @infExact, hyp.meanfunc, hyp.covfunc, hyp.likfunc, x', v2', xsample');
vopt = [vfunc1(xopt) vfunc2(xopt)];

% Plot results
if d==1
    plot_1D(rx, hyp, x, y, xopt, yopt)
end

end

function plot_1D(rx, hyp, x, y, xopt, yopt)

% resample GP
ntest = 10000;
d = size(rx,2);
xtest = repmat(rx(1,:)',1,ntest) + repmat(rx(2,:)'-rx(1,:)',1,ntest).*sort(rand(d,ntest));
[ytest, s2] = gp(hyp, @infExact, hyp.meanfunc, hyp.covfunc, hyp.likfunc, x', y', xtest');

% plot sampled reward function
figure(2); clf; hold on
plot(x, y, '.k', 'markersize',1);
plot(xopt, yopt, 'xb','markersize',10)
plot(xtest', ytest, 'r');
fill([xtest';flipud(xtest')],[ytest+sqrt(s2);flipud(ytest-sqrt(s2))],...
    [1 0.75 0.75],'EdgeColor','r','FaceAlpha',0.1,'EdgeAlpha',0.3);
xlabel('theta'); ylabel('reward');
axis([0 10 floor(min(ytest)) 0])

end

function [xopt, yopt] = find_max(rx, func)

d = size(rx,2);
ntest = 1000^d;
xtest = repmat(rx(1,:)',1,ntest) + repmat(rx(2,:)'-rx(1,:)',1,ntest).*rand(d,ntest);
ytest = func(xtest);

[yopt, iopt] = max(ytest);
xopt = xtest(:,iopt);

% % Direct optimization hyperparameters
% opt.tol = 1e-5; opt.mxevals = 100; opt.maxits = 100; opt.maxdeep = 100;
% opt.testflag = 0; opt.globalmin = -Inf; opt.showits = 0;

% % direct search
% problem.f = @(x) -rfunc(x);
% evalc('[~, xopt] = opt_direct(problem, rx'', opt);'); 
% yopt = rfunc(xopt);

end

