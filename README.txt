This is the code used to generate the results for the paper:
Lepora, N.F., 2016. Threshold Learning for Optimal Decision Making. In Advances in Neural Information Processing Systems (pp. 3756-3764).
http://papers.nips.cc/paper/6494-threshold-learning-for-optimal-decision-making

Matlab scripts should be runnable provided the following toolboxes are downloaded
https://www.mathworks.com/matlabcentral/fileexchange/23629-export-fig
http://gaussianprocess.org/gpml/code/matlab/release/gpml-matlab-v3.4-2013-11-11.zip
(note that startup needs to run at the start of each session before the gpml toolbox can be used - this will happen automatically if the path has been set)
Personally I download both to my MATLAB directory and set paths to each.

There are two directories:
\papercode
optimization functions (Bayes opt., REINFORCE, exhaustive samping)
plot scripts for the reward function in 1D and 2D
reward function for drift-diffusion model (inputs: thresholds and cost parameters)
sensitivity scripts for generating optimal parameters over multiple runs (Bayes opt., REINFORCE, exhaustive samping)
test scripts for single run (Bayes opt., REINFORCE, exhaustive samping)

\paperfigs
scripts for generating figure 1 to figure 6 in the paper from saved .mat files generated in \papercode
export-fig can be commented out if paper-quality figures are not needed

The above code is supplied without support.
N. Lepora, Dec. 2016


 
