clear all; clc
run startup

fname1 = 'sensitivity_bayes1.mat';
fname2 = 'sensitivity_sampling.mat';

figure(5); clf
let = {'A','B','C','D'};
xlab = 'cost parameter, c/W_1=c/W_0';
ylab = {'decision error, e','decision time, T','reward, R','threshold, \theta_1=\theta_0'};
tit = {'Decision accuracy','Decision time','Reward','Decision threshold'};
xvar = {'vals','vals','vals','vals'};
yvar = {'v1','v2','r','x1'};
xpos = 0.1:0.22:0.9; dxpos = 0.15;

for iplot = 4:-1:1

    load(fname2)
    eval(['xopt = ' xvar{iplot} '(:);']); 
    eval(['yopt = ' yvar{iplot} '(:);']); 

    load(fname1)
    eval(['x = ' xvar{iplot} '(:);']); 
    eval(['y = ' yvar{iplot} '(:);']); 
    
    if iplot==1; ly = 0; my = 0.5; end
    if iplot==2; ly = 0; my = 20; end
    if iplot==3; ly = -1; my = 0; end
    if iplot==4; ly = 0; my = 10; end
    lx = 0; mx = 0.1;
        
    hyp1.cov = [log(0.01); log(1)]; hyp1.lik = log(1);
    hyp1 = minimize(hyp1, @gp, -100, @infExact, [], @covSEiso, @likGauss, x, y);
    xtest = linspace(lx,mx,100)';
    [ytest, s2] = gp(hyp1, @infExact, [], @covSEiso, @likGauss, x, y, xtest);
        
    subplot(1,5,iplot); grid on; hold on; box on;
    plot(-1,-1,'b',-1,-1,'w',-1,-1,'r',-1,-1,'w')
    
    fill([xtest;flipud(xtest)],[ytest+sqrt(s2);flipud(ytest-sqrt(s2))],...
        [1 0.75 0.75],'EdgeColor','r','FaceAlpha',0.1,'EdgeAlpha',0.3);
    
    plot(xopt,yopt,'-b','linewidth',0.5)
    plot(xtest, ytest, 'r', 'LineWidth',1);
    plot(x,y,'.k','markersize',3); 
        
    set(gca,'fontsize',6,'xtick',0:0.02:0.1)
    xlabel(xlab,'fontsize',7)
    ylabel(ylab{iplot},'fontsize',7)
    title(tit{iplot},'fontsize',8)
    axis([lx mx ly my]);
    
    set(gca,'position',[xpos(iplot) 0.2 dxpos 0.65],'units','normalized')
    text(-0.3,1.1,let{iplot},'Units','Normalized','Fontsize',10,'Fontweight','bold')

    if iplot==4; hl = legend({'exhaustive','optimization','Bayesian','optimization'}); set(hl,'box','off','location','northwest'); end
    
    if iplot==4; sd = mean(sqrt(s2)); end
end

% export figure
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 7 1.75])
eval(['export_fig ' mfilename '.jpg -r300 -painters']);
% eval(['export_fig ' mfilename '.eps -r300 -painters']);

display([sd time/200 1/500*time/200])