clear all; clc

load('test_sampling.mat','xopt','vopt','fopt')
x1opt = xopt(1); x2opt = xopt(2); 
v1opt = vopt(1); v2opt = vopt(2); 
ropt = fopt;

load('test_reinforce.mat')

% Apply smoothing filter
span = 51;
t = 1:nt; 
r = smooth(fopt,span);
x1 = smooth(xopt(1,:),span);
x2 = smooth(xopt(2,:),span);
v1 = smooth(vopt(1,:),span);
v2 = smooth(vopt(2,:),span);

% plots
figure(2); clf
xpos = 0.1:0.23:0.9; dxpos = 0.16;
let = {'A','B','C','D','E'};
xlab = {'trials, N','trials, N','trials, N','trials, N','trials, N'};
ylab = {'decision error, e','decision time, T','reward, R','thresholds'};
tit = {'Decision accuracy','Decision time','Reward','Decision threshold'};
xvar = {'t','t','t','t','t'};
yvar = {'v1','v2','r','x1','x2'};
yoptvar = {'v1opt','v2opt','ropt','x1opt','x2opt'};

for iplot = 1:3
    
    eval(['x = ' xvar{iplot} '(:);']); lx = min([0; x]); mx = max(x)+eps;
    eval(['y = ' yvar{iplot} '(:);']); ly = min([0; y]); my = max(y*1.1)+eps;
    eval(['yopt = ' yoptvar{iplot} '(:);']);
    
    if iplot==1; ly = 0; my = 0.5; end
    if iplot==2; ly = 0; my = 20; end
    if iplot==3; ly = -1; my = 0; end
    
    subplot(1,4,iplot); grid on; hold on; box on;
    plot(x,y,'k');
    plot([lx mx],yopt*[1 1],':k')
    set(gca,'fontsize',6,'xtick',0:1000:5000)
    xlabel(xlab{iplot},'fontsize',7)
    ylabel(ylab{iplot},'fontsize',7)
    title(tit{iplot},'fontsize',8)
    axis([lx mx ly my]);
    set(gca,'position',[xpos(iplot) 0.2 dxpos 0.65],'units','normalized')
    text(-0.3,1.1,let{iplot},'Units','Normalized','Fontsize',10,'Fontweight','bold')
    
    if iplot==1
        modelFun =  @(p,x) p(3)*wblcdf(x,p(1)^2,p(2)^2);  
        startingVals = [sqrt(1000) 1 0.3 0.3]; %[10 2 5];
        [coefEsts,R] = nlinfit(x, y, modelFun, startingVals);
        plot(x, modelFun(coefEsts, x), 'r');
        R2 = 1 - sum(R.^2)/sum((y-mean(y)).^2);
    end
end

iplot = 4;
eval(['x(:,1) = ' xvar{4} '(:);']); lx = min([0; x]); mx = max(x)+eps;
eval(['y(:,1) = ' yvar{4} '(:);']); ly = min([0; y]); my = 10;
eval(['x(:,2) = ' xvar{5} '(:);']); 
eval(['y(:,2) = ' yvar{5} '(:);']);

subplot(1,4,iplot); grid on; hold on; box on;
plot(-1,-1,'b',-1,-1,'g')
plot(x,y(:,2),'b');
plot(x,y(:,1),'g');
plot([lx mx],x1opt*[1 1],':g');
plot([lx mx],x2opt*[1 1],':b');
set(gca,'fontsize',6,'xtick',0:1000:5000)
xlabel(xlab{iplot},'fontsize',7)
ylabel(ylab{iplot},'fontsize',7)
title(tit{iplot},'fontsize',8)
axis([lx mx ly my]);
set(gca,'position',[xpos(iplot) 0.2 dxpos 0.65],'units','normalized')
text(-0.25,1.1,let{iplot},'Units','Normalized','Fontsize',10,'Fontweight','bold')
hl = legend('\theta_1','\theta_0'); set(hl,'box','off')

% export figure
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 7 1.75])
eval(['export_fig ' mfilename '.jpg -r300 -painters']);
% eval(['export_fig ' mfilename '.eps -r300 -painters']);
