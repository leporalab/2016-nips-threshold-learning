clear all; clc

% set up run
c = 0.01; w = 1; cost = @(RT,e,c,w) c*RT + w*e;

% initialise data
ntrials = 10000;
nt = 500;    % max # time samples
si = 1;      % standard n of noise
mu = [1 -1]/3; ntargets = length(mu);
si = si*ones(ntargets,1);

for itrial = 1:ntrials
    target(itrial) = 1;
    s = mu(target(itrial)) + si(target(itrial))*randn(nt,1);
    z(:,itrial) = cumsum(s);
end

save(mfilename)
load(mfilename)

th = 3;
itrial = 7;
xpos = 0.14:0.44:0.9; dxpos = 0.33;
let = {'A','B','C','D'};

figure(1); clf
subplot(1,2,1); hold on; box on; grid on
plot(z(:,itrial),'.-k','markersize',5)    
plot([0 30],th*[1 1],':r')
plot([0 30],-th*[1 1],':r')
set(gca,'fontsize',7, 'Xtick',0:5:30,'Ytick',-10:5:10)
xlabel('decision time, t','fontsize',8)
ylabel('evidence, z','fontsize',8)
text(15,th-1,'threshold, \theta_1','fontsize',7)
text(14,-th-1,'threshold, -\theta_0','fontsize',7)
axis([0 30 -10 10])
title('Drift-diffusion model','fontsize',9)
set(gca,'position',[xpos(1) 0.25 dxpos 0.65],'units','normalized')
text(-0.25,1.11,let{1},'Units','Normalized','Fontsize',10,'Fontweight','bold')
 
load('..\optimization\plot_reward_ddf_1D')
subplot(1,2,2); hold; grid; box
plot(x, y, '.k','markersize',1);
plot(xtest', ytest, 'r');
[ymax,imax] = max(ytest);
plot(xtest(imax)*[1 1],[-2 ymax],':r','linewidth',1.5)
text(xtest(imax)+0.2,-1.65,{'optimal threshold','\theta_0^*=\theta_1*'},'fontsize',7)
set(gca,'fontsize',7, 'Xtick',0:2:10,'Ytick',-3:0.5:0)
axis([0 10 -2 0])
xlabel('equal thresholds, \theta_0=\theta_1','fontsize',8);
ylabel('reward, R','fontsize',8);
title('Reward with threshold','fontsize',9)
set(gca,'position',[xpos(2) 0.25 dxpos 0.65],'units','normalized')
text(-0.23,1.11,let{2},'Units','Normalized','Fontsize',10,'Fontweight','bold')

% export figure
path(path,'export_fig') % remember pdftops in export_fig too
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 4.5 1.5])
eval(['export_fig ' mfilename '.jpg -r300 -painters']);
% eval(['export_fig ' mfilename '.eps -r300 -painters']);
