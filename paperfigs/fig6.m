clear all; clc

load('sensitivity_reinforce_2D')

% %%%%%%%%%%% plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(3); clf
xpos = 0.1:0.22:0.9; xpos(5:6) = xpos(1:2); dxpos = 0.15;
ypos = [0.6375*ones(1,4) 0.18*[1 1]];
let = {'A','B','C','D','E','F'};
blab = {'decision error , e','decision time, T','reward, R','threshold, \theta_1'};
tit = {'Decision accuracy','Decision time','Reward','Decision threshold'};
yvar = {'v1','v2','r','x1'};
lzs = [0 0 -0.5 0];
mzs = [0.5 20 0 10];

for iplot = 1:4
  
  x = vals(:,1); lx = min([0; x]); mx = max([0; x]);
  y = vals(:,2); ly = min([0; y]); my = max([0; y]);
  eval(['z = ' yvar{iplot} '(:);']); lz = lzs(iplot); mz = mzs(iplot);
  z = reshape(z,sqrt(nruns)*[1 1]);
  
  subplot(2,4,iplot); grid on; hold on; box on;
  imagesc(x,y,z,[lz mz]); 
  cm = colormap('hot'); cm = flipud(cm); colormap(cm);
  axis([lx,mx,ly,my])
  set(gca,'XTick',0:0.02:0.1,'YTick',0:0.02:0.1)
  rz{iplot} = linspace(lz,mz,6);
  
  set(gca,'fontsize',6)
  title(tit{iplot},'fontsize',8)
  xlabel('cost parameter, c/W_0','fontsize',6); ylabel('cost parameter, c/W_1','fontsize',6);
  set(gca,'position',[xpos(iplot) 0.38 dxpos 0.5],'units','normalized')
  text(-0.375,1.1,let{iplot},'Units','Normalized','Fontsize',10,'Fontweight','bold')
  
  hc = colorbar; delete(hc)
  c_vect = caxis;
  cbar{iplot}=colorbar('southoutside');
  cpos=get(cbar{iplot},'Position');
  cpos(1) = cpos(1); cpos(3) = cpos(3); cpos(4) = cpos(4)*0.5;
  cpos(2) = cpos(2)*0.3; if iplot>4; cpos(2) = cpos(2)*0.4; end
  set(cbar{iplot},'Position',cpos,'fontsize',6)
  text(0.05,-0.0625,blab{iplot},'fontsize',6,'HorizontalAlignment','center');
  
end

for iplot = 1:4; set(cbar{iplot},'xtick',rz{iplot}); end
  
% export figure
path(path,'export_fig') % remember pdftops in export_fig too
set(gcf, 'color', 'w', 'units', 'inches', 'position', [0 0 7 2])
eval(['export_fig ' mfilename '.jpg -r600 -painters']);
% eval(['export_fig ' mfilename '.eps -r300 -painters']);

% th1 = log((1-res.al1)./res.al2);
% th2 = log(res.al1./(1-res.al2));
%
% figure(10); clf
% subplot(2,2,1); plot(res.al1,'b')
% subplot(2,2,2); plot(res.al2,'b')
% subplot(2,2,3); hold; plot(res.b1,'b'); plot(th1,'r')
% subplot(2,2,4); hold; plot(res.b2,'b'); plot(-th2,'r')
% figure(11); clf; hold on
% subplot(1,2,1); plot(res.al2./res.al1,res.vals(:,1)./res.vals(:,2),'.b'); axis([0 10 0 10])
% subplot(1,2,2); hold on; plot(res.al2./res.al1); plot(res.vals(:,1)./res.vals(:,2),'r');
% % plot(,'r')
